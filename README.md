# Kahoot mock up in C++ and Qt framework!

## Description
This is a basic quiz game loosley based on the popular quiz application [Kahoot](https://kahoot.com/).

## Table of Contents

- [Background](#background)
- [Implementation](#implementation)
- [Install](#install)
- [Usage](#usage)
- [Contributing](#contributing)
- [License](#license)

## Background
The quiz gamle is loosely based upon Kahoot. The user will get 3 different questions and get points for each correct answer.
The player has 10 seconds to complete each question, if he/she does not answer in time next question or the last page will be displayed.
At the end total points together with average answer time will be displayed.

## Implementation
The quiz game was built using the [Qt](https://www.qt.io/) framework along with C++. Qmake is used in Qt.


## Install
- Clone the repo
- [Download Qt](https://www.qt.io/download)

## Usage
Simply press on Run in Qt to run the application and the game will start.

- **Welcome**
![](welcome.PNG)

- **Add player name**
![](enter_name.PNG)

- **Answer questions** 
![](start_playing.PNG)

- **Waiting for next question** 
![](next_game.PNG)

- **Get total points and average time**
![](end_game.PNG)

<br/>

## Contributing
This project has been created by Helena Barmer.
Not accepting contributors at the moment. Feel free to use the code and implement it in your own creative way.

## License
[Qt open source license](https://www.qt.io/download-open-source?hsCtaTracking=9f6a2170-a938-42df-a8e2-a9f0b1d6cdce%7C6cb0de4f-9bb5-4778-ab02-bfb62735f3e5)
<br/>
MIT
