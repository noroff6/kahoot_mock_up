#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // Set window size to 70% of screen
    resize(QGuiApplication::primaryScreen()->availableGeometry().size() * 0.7);

    // Change window title
    setWindowTitle("Welcome to the quiz game! ");

    // Set up timer
    timer = new QTimer(this);
    progress_timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(on_lcdNumber_overflow()));
    connect(progress_timer, SIGNAL(timeout()), this, SLOT(on_progressbar_overflow()));

    ui->pushButton->setStyleSheet("QPushButton#pushButton{background-color: white; border: none; transition-duration: 0.2s;"
                                  "border: 2px solid #FF00FF;}"
                                  "QPushButton#pushButton:hover{background-color: #FF00FF; color: white;}"
                                  "QPushButton#pushButton{font-family: Courier New;}"
                                  "QPushButton#pushButton{font-size: 14pt;}");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::read_file(QString filename, int index)
{
    QFile file(filename);
    if(!file.open(QIODevice::ReadOnly)) {
        QMessageBox::information(0, "error", file.errorString());
    }

    QTextStream in(&file);

    // Vector for text
    QVector<QString>quiz_text;

    // Add text to vector
    for (int i = 0; i < 5; ++i)
    {
        QString line = in.readLine();
        quiz_text.append(line);
    }

    // Assign questions to page
    ui->stackedWidget->setCurrentIndex(index);
    ui->question->setText(quiz_text[0]);
    ui->option_1->setText(quiz_text[1]);
    ui->option_2->setText(quiz_text[2]);
    ui->option_3->setText(quiz_text[3]);
    ui->option_4->setText(quiz_text[4]);

    file.close();
}



void MainWindow::play_game(QString name, QString filename, int index, int points)
{
    countdown = 11;
    progress_count = 4;
    read_file(filename, index);
    ui->stackedWidget->setCurrentIndex(3);
    ui->player_name->setText(name);
    QString str_points = QString::number(points);
    ui->total_points->setText(str_points);
    ui->lcdNumber->display("");
    timer->start(1000);
}

void MainWindow::in_between_games()
{
    ui->stackedWidget->setCurrentIndex(2);
    //ui->lcdNumber_2->display("");
    ui->next_game_name->setText("Get ready for the next question " + name + "!");
    progress_timer->start(1000);
}

void MainWindow::next_game()
{

    QString str_points = QString::number(points);
    ui->total_points->setText(str_points);
    current_question_page++;
    QString str_current_question_page = QString::number(current_question_page);
    QString file = "C:/Users/HBarme/Documents/Kahoot/question" + str_current_question_page + ".txt";
    //timer->stop();

    play_game(name, file, 3, points);
}


void MainWindow::on_pushButton_clicked()
{
    name = ui->input_name->text();

    if(!name.isEmpty())
    {
        play_game(name, "C:/Users/HBarme/Documents/Kahoot/question1.txt", 2, points);
        current_question_page = 1;
    }
    else
    {
        QMessageBox::warning(this, "Warning" ,"Please add a name. Field cannot be empty. ");
    }
}


void MainWindow::on_input_name_textChanged(const QString &arg1)
{
    animation = new QPropertyAnimation(ui->input_name, "geometry");
    animation->setDuration(300);
    animation->setStartValue(ui->input_name->geometry());
    animation->setEndValue(QRect(320, 250, 250, 50));
    animation->start();
    ui->input_name->setAlignment(Qt::AlignCenter);
    ui->input_name->setStyleSheet("color: black; "
                                  "background-color:#FF55FF; "
                                  "font-family: Chiller; font-size: 40px;"
                                  "border-Width: 3px;"
                                  "border-style:solid;"
                                  "border-color: black;"
                                  "font-weight: bold;");

    // Max length of input name
    ui->input_name->setMaxLength(20);
    // TODO: Add regex to arg1
}

// Timer for questions
void MainWindow::on_lcdNumber_overflow()
{
    countdown--;
    ui->lcdNumber->display(countdown);
    if(countdown == 0)
    {
        timer->stop();
        points = 0;
        if(current_question_page == 3)
        {
            last_page();
        }
        else
        {
          in_between_games();
        }
    }
}

void MainWindow::last_page()
{
    qDebug() << average_time;
    average_time/=current_question_page;
    qDebug() << "Average total time: " << average_time;
    qDebug() << "Current question page: "  <<current_question_page;
    //timer->stop();
    ui->stackedWidget->setCurrentIndex(1);
    QString str_points = QString::number(points);
    QString str_time = QString::number(average_time);
    ui->exit_player_name->setText(name);
    ui->total_points->setText(str_points);
    ui->exit_points->setText(str_points);
    ui->exit_time->setText(str_time);

    // Progressbar points
    ui->progressbar_points->setMaximum(3);
    ui->progressbar_points->setValue(points);

    // Progressbar time
    ui->progressbar_time->setMaximum(10);
    ui->progressbar_time->setValue(average_time);


}

// Last page after clicking Exit button
void MainWindow::on_pushButton_3_clicked()
{
    timer->stop();
    last_page();
}


void MainWindow::on_button_A_clicked()
{
    timer->stop();
    average_time+=countdown;
    average_time-=1;

    if(current_question_page == 2)
    {
        points++;
        in_between_games();
    }
    else if(current_question_page == 3)
    {
        last_page();
    }
    else
    {
      in_between_games();
    }

}


void MainWindow::on_button_B_clicked()
{
    timer->stop();
    average_time+=countdown;
    average_time-=1;

    if(current_question_page == 3)
    {
        points++;
        last_page();
    }
    else
    {
        in_between_games();
    }
}


void MainWindow::on_button_C_clicked()
{
    timer->stop();
    average_time+=countdown;
    average_time-=1;

    if(current_question_page == 1)
    {
        points++;
        in_between_games();
    }
    else if(current_question_page == 3)
    {
        last_page();
    }
    else
    {
        in_between_games();
    }
}


void MainWindow::on_button_D_clicked()
{
    timer->stop();
    average_time+=countdown;
    average_time-=1;

    if(current_question_page == 3)
    {
        last_page();
    }
    else
    {
        in_between_games();
    }

}

// Stop application and exit game
void MainWindow::on_pushButton_2_clicked()
{
    qApp->quit();
}


void MainWindow::on_progressbar_overflow()
{

    progress_count--;
    int step = ui->progressBar->maximum() / 4;
    int current = ui->progressBar->maximum() - (progress_count * step);
    ui->progressBar->setValue(current);
    if(progress_count == 0)
    {
        progress_timer->stop();
        ui->progressBar->reset();
        next_game();
    }
}

