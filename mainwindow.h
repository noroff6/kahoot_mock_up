#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtWidgets>
#include <QTimer>
#include <QPropertyAnimation>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_pushButton_clicked();
    void on_input_name_textChanged(const QString &arg1);
    void on_lcdNumber_overflow();
    void on_pushButton_3_clicked();

    void on_button_A_clicked();

    void on_button_B_clicked();

    void on_button_C_clicked();

    void on_button_D_clicked();

    void on_pushButton_2_clicked();

    void on_progressbar_overflow();

private:
    Ui::MainWindow *ui;
    QString name;
    int points = 0;
    int countdown = 11;
    int progress_count = 4;
    int average_time = 0;
    int current_question_page;
    QTimer *timer = nullptr;
    QTimer *progress_timer = nullptr;
    QPropertyAnimation *animation = nullptr;
    void next_game();
    void read_file(QString filename, int index);
    void play_game(QString name, QString filename, int index, int points);
    void last_page();
    void in_between_games();

};
#endif // MAINWINDOW_H
